const purgecss = require("@fullhuman/postcss-purgecss");

module.exports = {
  style: {
    postcss: {
      plugins: [
        purgecss({
          content: ["./public/index.html", "./src/**/*.tsx", "./src/**/*.ts"],
          whitelistPatterns: [/^ct-/],
        }),
      ],
    },
  },
};
