import React, { createContext, useCallback, useContext, useState } from "react";
import { useDebounce } from "react-use";
import { useFinnhub } from ".";
import { Category, Stock, Price } from "../types";

interface CompanyProfile {
  country: string;
  currency: string;
  exchange: string;
  ipo: string;
  marketCapitalization: number;
  name: string;
  phone: string;
  shareOutstanding: number;
  ticker: string;
  weburl: string;
  logo: string;
  finnhubIndustry: string;
}

interface State {
  categories: Category[];
  stocks: Stock[];
  prices: Record<string, Price>;
}

const Context = createContext(
  {} as {
    categories: Category[];
    setCategory: (name: string, dispatch: (c: Category) => Category) => void;

    stocks: Stock[];
    setStock: (ticker: string, dispatch: (c: Stock) => Stock) => void;

    getPrice: (symbol: string) => Price | undefined;
    setPrice: (symbol: string, value: number) => void;
    refreshPrices: () => void;

    addStock: (ticker: string) => Promise<void>;
    removeStock: (ticker: string) => void;

    exportState: () => State;
    importState: (state: State) => void;
  }
);

export const AppStateProvider: React.FC = ({ children }) => {
  const [categories, setCategories] = useState(
    JSON.parse(localStorage.getItem("categories") ?? "[]") as Category[]
  );
  const [stocks, setStocks] = useState(
    JSON.parse(localStorage.getItem("stocks") ?? "[]") as Stock[]
  );
  const [prices, setPrices] = useState(
    JSON.parse(localStorage.getItem("prices") ?? "{}") as Record<
      string,
      Price | undefined
    >
  );

  const setCategory = useCallback(
    (name: string, dispatch: (c: Category) => Category) => {
      setCategories((categories) =>
        categories.map((category) =>
          category.name === name ? dispatch(category) : category
        )
      );
    },
    []
  );

  const setStock = useCallback(
    (ticker: string, dispatch: (s: Stock) => Stock) => {
      setStocks((stocks) =>
        stocks.map((stock) =>
          stock.ticker === ticker ? dispatch(stock) : stock
        )
      );
    },
    []
  );

  const getPrice = useCallback((symbol: string) => prices[symbol], [prices]);
  const setPrice = useCallback(
    (symbol: string, value: number) =>
      setPrices((prices) => ({
        ...prices,
        [symbol]: { value, updated: Date.now() },
      })),
    []
  );
  const refreshPrices = useCallback(() => setPrices({}), [setPrices]);

  const client = useFinnhub();
  const addStock = async (symbol: string) => {
    if (stocks.find(({ ticker }) => ticker === symbol.toUpperCase())) return;
    const { ticker, name, logo, finnhubIndustry: category } = await client
      .get<CompanyProfile>("/stock/profile2", { params: { symbol } })
      .then(({ data }) => data);

    const stock = {
      ticker,
      name,
      logo,
      category,
      weight: 0,
      shares: 0,
    } as Stock;
    if (!ticker || !name) throw new Error("Ticker not found");

    setStocks((stocks) => [...stocks, stock]);
    if (!categories.find(({ name }) => name === category)) {
      setCategories((categories) => [
        ...categories,
        { name: category, weight: 0 },
      ]);
    }
  };

  const removeStock = (symbol: string) => {
    setStocks((stocks) => {
      const stock = stocks.find(({ ticker }) => ticker === symbol);
      if (!stock) return stocks;

      setCategories((categories) => {
        const next = stocks.find(
          ({ ticker, category }) =>
            ticker !== symbol && category === stock.category
        );
        if (next) return categories;
        return categories.filter(({ name }) => name !== stock.category);
      });
      return stocks.filter(({ ticker }) => ticker !== symbol);
    });
  };

  const exportState = useCallback(
    () => ({ categories, stocks, prices } as State),
    [categories, stocks, prices]
  );
  const importState = useCallback(({ categories, stocks, prices }: State) => {
    for (let i in categories) {
      const { name, weight } = categories[i];
      if (typeof name !== "string")
        throw new Error(`Invalid category name at index ${i}: must be string`);
      if (
        typeof weight !== "number" ||
        !Number.isSafeInteger(weight) ||
        weight < 0
      )
        throw new Error(
          `Invalid category weight at index ${i}: must be non-negative integer`
        );
    }

    const categoryNames = new Set(categories.map(({ name }) => name));
    for (let i in stocks) {
      const { category, name, logo, shares, ticker, weight } = stocks[i];
      if (!category || typeof category !== "string")
        throw new Error(
          `Invalid stock category at index ${i}: must be non-empty string`
        );
      if (!categoryNames.has(category))
        throw new Error(
          `Invalid stock category at index ${i}: category ${category} not found`
        );
      if (!name || typeof name !== "string")
        throw new Error(
          `Invalid stock name at index ${i}: must be non-empty string`
        );
      if (typeof logo !== "string")
        throw new Error(`Invalid stock logo at index ${i}: must be string`);
      if (
        typeof shares !== "number" ||
        !Number.isSafeInteger(shares) ||
        shares < 0
      )
        throw new Error(
          `Invalid stock shares at index ${i}: must be non-negative integer`
        );
      if (!ticker || typeof ticker !== "string")
        throw new Error(
          `Invalid stock ticker at index ${i}: must be non-empty string`
        );
      if (
        typeof weight !== "number" ||
        !Number.isSafeInteger(weight) ||
        weight < 0
      )
        throw new Error(
          `Invalid stock weight at index ${i}: must be non-negative integer`
        );
    }

    const tickers = new Set(stocks.map(({ ticker }) => ticker));
    for (let [key, val] of Object.entries(prices)) {
      if (!tickers.has(key))
        throw new Error(`Invalid price ticker ${key}: stock not found`);
      if (typeof val.value !== "number" || val.value < 0)
        throw new Error(
          `Invalid price value at ticker ${key}: must be non-negative number`
        );
      if (typeof val.updated !== "number")
        throw new Error(
          `Invalid price updated at ticker ${key}: must be number`
        );
    }

    setCategories(categories);
    setStocks(stocks);
    setPrices(prices);
  }, []);

  useDebounce(
    () => localStorage.setItem("categories", JSON.stringify(categories)),
    1e3,
    [categories]
  );
  useDebounce(
    () => localStorage.setItem("stocks", JSON.stringify(stocks)),
    1e3,
    [stocks]
  );
  useDebounce(
    () => localStorage.setItem("prices", JSON.stringify(prices)),
    1e3,
    [prices]
  );

  return (
    <Context.Provider
      value={{
        categories,
        setCategory,
        stocks,
        setStock,
        addStock,
        removeStock,
        getPrice,
        setPrice,
        refreshPrices,
        exportState,
        importState,
      }}
    >
      {children}
    </Context.Provider>
  );
};

export const useAppState = () => useContext(Context);
