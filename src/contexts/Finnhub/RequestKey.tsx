import React, { Dispatch, SetStateAction, useState } from "react";
import { useAsyncFn, useCss } from "react-use";
import background from "../../margot.webp";

const RequestKey: React.FC<{
  setToken: Dispatch<SetStateAction<string | undefined>>;
}> = ({ setToken }) => {
  const [value, setValue] = useState("");
  const [{ loading }, onSubmit] = useAsyncFn(async () => {
    const url = new URL("/api/v1/quote", "https://finnhub.io");
    url.searchParams.set("symbol", "AAPL");
    url.searchParams.set("token", value);
    await fetch(url.href);
    setToken(value);
  }, [value]);

  const className = useCss({
    background: `linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(${background}) center / cover no-repeat`,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
    width: "100vw",

    ".label": { color: "snow" },
  });

  return (
    <main className={className}>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          if (value) onSubmit();
        }}
      >
        <div className="field">
          <div className="field-label is-medium">
            <label className="label" htmlFor="token">
              API token
            </label>
          </div>
          <div className="field-body">
            <div className={`control is-medium ${loading ? "is-loading" : ""}`}>
              <input
                id="token"
                className="input is-medium"
                inputMode="search"
                value={value}
                onChange={(e) => setValue(e.currentTarget.value)}
                disabled={loading}
              />
            </div>
          </div>
        </div>
      </form>
    </main>
  );
};

export default RequestKey;
