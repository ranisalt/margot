import React, {
  createContext,
  useContext,
  useMemo,
  Suspense,
  lazy,
} from "react";
import { useLocalStorage } from "react-use";
import axios from "axios";
import rateLimit from "axios-rate-limit";

const RequestKey = lazy(() => import("./RequestKey"));

const makeFetcher = (token: string) => {
  const fetcher = rateLimit(
    axios.create({ baseURL: "https://finnhub.io/api/v1/" }),
    { maxRequests: 25, perMilliseconds: 1000 }
  );
  fetcher.interceptors.request.use((config) => ({
    ...config,
    params: { ...config.params, token },
  }));
  return fetcher;
};

const Context = createContext({} as ReturnType<typeof makeFetcher>);

export const FinnhubProvider: React.FC = ({ children }) => {
  const [token = "", setToken] = useLocalStorage<string>("token", undefined, {
    raw: true,
  });
  const client = useMemo(() => makeFetcher(token), [token]);

  if (!token) {
    return (
      <Suspense fallback={null}>
        <RequestKey setToken={setToken} />
      </Suspense>
    );
  }

  return <Context.Provider value={client}>{children}</Context.Provider>;
};

export const useFinnhub = () => useContext(Context);
