import React, {
  Dispatch,
  SetStateAction,
  createContext,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { useAppState } from ".";

const Context = createContext(
  {} as {
    totalWorth: number;
    setCash: Dispatch<SetStateAction<number>>;
    getToBuy: (ticker: string) => number;
  }
);

export const SuggestionProvider: React.FC = ({ children }) => {
  const { categories, stocks, getPrice } = useAppState();
  const [cash, setCash] = useState(
    JSON.parse(localStorage.getItem("cash") ?? "0") as number
  );
  useEffect(() => localStorage.setItem("cash", JSON.stringify(cash)), [cash]);

  const totalWorth = useMemo(
    () =>
      stocks.reduce((prev, { ticker, shares = 0 }) => {
        const { value = 0 } = getPrice(ticker) ?? {};
        return prev + shares * value;
      }, 0),
    [getPrice, stocks]
  );

  const toBuy = useMemo(() => {
    let remaining = Number(cash) || 0;
    if (remaining === 0) return {};
    const totalValue = totalWorth + remaining;

    const totalWeight = categories.reduce(
      (prev, { weight = 0 }) => prev + weight,
      0
    );
    const weightByCategory = Object.fromEntries(
      categories.map(({ name, weight }) => [name, weight])
    );
    const innerWeightByCategory = Object.fromEntries(
      categories.map(({ name }) => [
        name,
        stocks
          .filter(({ category }) => category === name)
          .reduce((prev, { weight = 0 }) => prev + weight, 0),
      ])
    );

    const stockState = Object.fromEntries(
      stocks
        .filter(
          ({ category, weight = 0 }) =>
            weight !== 0 && weightByCategory[category] !== 0
        )
        .map((stock) => [stock.ticker, { ...stock }] as [string, typeof stock])
        .filter(([ticker]) => {
          const { value = 0 } = getPrice(ticker) ?? {};
          return value !== 0;
        })
    );

    const categoryCurrentValue = (category: string) =>
      Object.values(stockState)
        .filter((stock) => category === stock.category)
        .reduce((sum, { ticker, shares = 0 }) => {
          const { value = 0 } = getPrice(ticker) ?? {};
          return sum + shares * value;
        }, 0) / totalValue;
    const categoryExpectedValue = (category: string) =>
      weightByCategory[category] / totalWeight;
    const categoryDeviation = (category: string) =>
      categoryCurrentValue(category) - categoryExpectedValue(category);

    const stockCurrentValue = (ticker: string) => {
      const { shares = 0 } = stockState[ticker];
      const { value = 0 } = getPrice(ticker) ?? {};
      return (shares * value) / totalValue;
    };
    const stockExpectedValue = (ticker: string) => {
      const { category, weight = 0 } = stockState[ticker];
      return (
        (weight / innerWeightByCategory[category]) *
        (weightByCategory[category] / totalWeight)
      );
    };
    const stockDeviation = (ticker: string) =>
      stockCurrentValue(ticker) - stockExpectedValue(ticker);
    const canBuy = ({ ticker }: typeof stocks[0]) => {
      const { value = 0 } = getPrice(ticker) ?? {};
      return remaining >= value;
    };

    const toBuy = {} as Record<string, number | undefined>;
    while (remaining > 0 && Object.keys(stockState).length !== 0) {
      const available = Object.values(stockState)
        .filter(canBuy)
        .sort((a, b) => {
          if (a.category === b.category) {
            return stockDeviation(a.ticker) - stockDeviation(b.ticker);
          }
          return categoryDeviation(a.category) - categoryDeviation(b.category);
        });
      if (available.length === 0) break; // if can't buy more
      const [{ ticker }] = available;
      if (stockDeviation(ticker) >= 0) break; // if current > expected
      const { value = 0 } = getPrice(ticker) ?? {};
      remaining -= value;
      toBuy[ticker] = (toBuy[ticker] ?? 0) + 1;
      stockState[ticker].shares += 1;
    }
    return toBuy;
  }, [categories, cash, getPrice, stocks, totalWorth]);

  return (
    <Context.Provider
      value={{ totalWorth, setCash, getToBuy: (ticker) => toBuy[ticker] ?? 0 }}
    >
      {children}
    </Context.Provider>
  );
};

export const useSuggestion = () => useContext(Context);
