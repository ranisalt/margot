import React from "react";
import { SWRConfig } from "swr";
import { StockList, Prices, Graphs } from "./components";
import { AppStateProvider, SuggestionProvider } from "./contexts";

const View: React.FC = () => (
  <AppStateProvider>
    <StockList />
    <SuggestionProvider>
      <SWRConfig
        value={{
          dedupingInterval: 60e3,
          focusThrottleInterval: 60e3,
          revalidateOnFocus: false,
        }}
      >
        <Prices />
      </SWRConfig>
      <Graphs />
    </SuggestionProvider>
  </AppStateProvider>
);

export default View;
