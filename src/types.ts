export interface Category {
  name: string;
  weight: number;
}
export interface Stock {
  ticker: string;
  name: string;
  logo: string;
  category: string;
  weight: number;
  shares: number;
}
export interface Price {
  value: number;
  updated: number;
}
