import React, { useEffect, useState } from "react";
import { useCss } from "react-use";
import useSWR from "swr";
import { useAppState, useFinnhub, useSuggestion } from "../contexts";
import { Stock } from "../types";

export const Prices: React.FC = () => {
  const className = useCss({
    gridArea: "prices",

    "[role='row']": {
      display: "flex",
      "> :not(:first-child)": { textAlign: "center" },
    },
    "[role='columnheader'], [role='cell']": {
      ".thead &, .tbody &": {
        borderBottom: "1px solid gainsboro",
      },
      padding: "0.25rem 0.5rem",
      flex: 1,
    },

    ".thead": {
      borderBottom: "1px solid lightgray",
      color: "dimgray",
      display: "fixed",
      fontWeight: 700,
    },

    ".refresh": {
      cursor: "pointer",
      margin: "0 0.25rem",
    },
    ".tbody": {
      height: "calc(100% - 54px)",
      overflowY: "scroll",
    },
    ".tbody > *": {
      borderTop: "1px solid gainsboro",
      flex: 1,
    },
    ".tbody .shares": {
      display: "flex",
      placeContent: "center",
      "> *": { margin: "0 0.5rem" },
    },
    ".tfoot": { borderTop: "1px solid lightgray" },
    ".tbody .buy:not(:empty)": { background: "rgba(25%, 75%, 25%, 0.3)" },
    ".dock": {
      flex: 2,
      "> *": { flex: "initial" },
    },
  });

  const { stocks, refreshPrices } = useAppState();
  const { totalWorth, setCash } = useSuggestion();
  const [localCash, setLocalCash] = useState("");

  return (
    <section className={`${className} is-size-7`} role="table">
      <div className="thead" role="row">
        <div role="columnheader">Ticker</div>
        <div role="columnheader">Shares</div>
        <div role="columnheader">Buy</div>
        <div role="columnheader">
          Price
          <span className="refresh" onClick={() => refreshPrices()}>
            {"\ud83d\uddd8"}
          </span>
        </div>
        <div role="columnheader">Worth</div>
      </div>
      <div className="tbody" role="rowgroup">
        {[...stocks]
          .sort((a, b) => a.ticker.localeCompare(b.ticker))
          .map((stock) => (
            <Price key={stock.ticker}>{stock}</Price>
          ))}
      </div>
      <div className="tfoot" role="row">
        <div role="cell">
          <strong>Total</strong>
        </div>
        <div className="dock">
          <form
            className="field is-horizontal"
            onSubmit={(e) => {
              e.preventDefault();
              setCash(Number(localCash) || 0);
              setLocalCash("");
            }}
          >
            <div className="field-label is-small">
              <label className="label" htmlFor="dock">
                Cash
              </label>
            </div>
            <div className="field-body">
              <div className="field has-addons">
                <div className="control">
                  <span className="button is-small is-static">US$</span>
                </div>
                <div className="control">
                  <input
                    id="dock"
                    className="input is-small"
                    inputMode="decimal"
                    value={localCash}
                    onChange={(e) => setLocalCash(e.currentTarget.value)}
                  />
                </div>
                <div className="control">
                  <button type="submit" className="button is-small is-primary">
                    Go
                  </button>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div role="cell">{`US$ ${totalWorth.toFixed(2)}`}</div>
      </div>
    </section>
  );
};

const Price: React.FC<{ children: Stock }> = ({ children: stock }) => {
  const { setStock, getPrice, setPrice } = useAppState();
  const client = useFinnhub();
  const { setCash, getToBuy } = useSuggestion();

  const { ticker, shares = 0 } = stock;
  const { value: initialData, updated = 0 } = getPrice(ticker) ?? {};
  const { data: value = 0, error } = useSWR(
    ["/quote", ticker],
    (url, symbol) =>
      client
        .get<{ c: number }>(url, { params: { symbol } })
        .then(({ data }) => data.c),
    {
      initialData,
      refreshInterval: 300e3, // 5 minutes
      revalidateOnMount: updated < Date.now() - 300e3,
    }
  );
  useEffect(() => {
    if (value) setPrice(ticker, value);
  }, [setPrice, ticker, value]);

  const buy = getToBuy(ticker);

  return (
    <div role="row">
      <div role="cell">
        <span>{stock.ticker}</span>
        {error && (
          <abbr title={error} className="icon">
            {"\u26a0"}
          </abbr>
        )}
      </div>
      <div className="shares" role="cell">
        <span>{shares}</span>
        <span
          className="icon is-small"
          onClick={() =>
            setStock(stock.ticker, ({ shares = 0, ...stock }) => ({
              ...stock,
              shares: shares + 1,
            }))
          }
          role="button"
        >
          {"\u2795"}
        </span>
        <span
          className="icon is-small"
          onClick={() =>
            setStock(stock.ticker, ({ shares = 0, ...stock }) => ({
              ...stock,
              shares: Math.max(shares - 1, 0),
            }))
          }
          role="button"
        >
          {"\u2796"}
        </span>
      </div>
      <div className="shares buy" role="cell">
        {buy !== 0 && (
          <>
            <span>{buy}</span>
            <span
              className="icon is-small"
              onClick={() => {
                setStock(stock.ticker, ({ shares = 0, ...stock }) => ({
                  ...stock,
                  shares: shares + buy,
                }));
                setCash((cash) => cash - value);
              }}
              role="button"
            >
              {"\u2714"}
            </span>
          </>
        )}
      </div>
      <div role="cell">{value ? `US$ ${value.toFixed(2)}` : "loading..."}</div>
      <div role="cell">
        {value ? `US$ ${(value * shares).toFixed(2)}` : "loading..."}
      </div>
    </div>
  );
};
