import React, { SyntheticEvent, useCallback, useEffect, useState } from "react";
import { useCss, useAsyncFn } from "react-use";
import { useAppState } from "../contexts";
import { Category, Stock } from "../types";

export const StockList: React.FC = () => {
  const className = useCss({
    background: "rebeccapurple",
    display: "flex",
    flexDirection: "column",
    gridArea: "aside",
    gap: "1rem",
    padding: "1rem 0",

    ".menu-list": {
      overflow: "auto",

      ".header": {
        display: "flex",
        gap: "0.5rem",
        alignItems: "center",
        padding: "0.5rem",
        marginBottom: "-0.5rem",
        transition: "0.15s background-color",

        "&:hover": { background: "rgba(255, 255, 255, 0.1)" },
        "> .name": { flexGrow: 1 },
      },

      "ul > li": {
        display: "flex",
        gap: "0.5rem",
        alignItems: "center",
        transition: "0.15s background-color",

        "> .name": { flexGrow: 1 },
        "> .remove:hover": { color: "red" },

        "&:hover": { background: "rgba(255, 255, 255, 0.1)" },
      },
    },

    img: {
      flexShrink: 0,
      height: "1rem",
      width: "1rem",

      // hide alt text
      overflow: "hidden",
      textIndent: "100%",
    },

    ".search, .buttons": { padding: "0 1rem" },
    ".buttons": {
      display: "flex",
      gap: "1rem",
      "& > div": { flex: 1 },
    },
    ".button": {
      width: "100%",
      "&:not(:first-child)": { marginLeft: "1rem" },
    },
    "input[type=file]": {
      position: "absolute",
      visibility: "hidden",
    },
  });
  const { categories } = useAppState();

  const sortedCategories = [...categories].sort((a, b) =>
    a.name.localeCompare(b.name)
  );
  return (
    <aside className={`${className} has-text-light is-clipped menu`}>
      <SearchTicker />

      {categories.length !== 0 && (
        <ul className="menu-list">
          {sortedCategories.map((category) => (
            <CategoryEntry key={category.name}>{category}</CategoryEntry>
          ))}
        </ul>
      )}

      <div className="buttons">
        <Export />
        <Import />
      </div>
    </aside>
  );
};

const CategoryEntry: React.FC<{ children: Category }> = ({
  children: category,
}) => {
  const { categories, setCategory, stocks } = useAppState();

  const totalWeight = categories.reduce((acc, { weight }) => acc + weight, 0);
  const percent = 100 * (category.weight / totalWeight);

  const sortedStocks = stocks
    .filter((stock) => stock.category === category.name)
    .sort((a, b) => a.ticker.localeCompare(b.ticker));
  if (sortedStocks.length === 0) return null;

  return (
    <li key={category.name}>
      <div className="header">
        <strong className="ellipsis has-text-white name">
          <abbr title={category.name}>{category.name}</abbr>
        </strong>
        <span className="is-size-7">
          <abbr
            title={`${category.weight} / ${totalWeight}`}
          >{`${percent.toFixed(2)}%`}</abbr>
        </span>
        <span
          className="icon is-small"
          onClick={() =>
            setCategory(category.name, ({ weight, ...category }) => ({
              ...category,
              weight: weight + 1,
            }))
          }
          role="button"
        >
          {"\u2795"}
        </span>
        <span
          className="icon is-small"
          onClick={() =>
            setCategory(category.name, ({ weight, ...category }) => ({
              ...category,
              weight: Math.max(weight - 1, 0),
            }))
          }
          role="button"
        >
          {"\u2796"}
        </span>
      </div>
      <ul>
        {sortedStocks.map((stock) => (
          <StockEntry key={stock.ticker}>{stock}</StockEntry>
        ))}
      </ul>
    </li>
  );
};

const StockEntry: React.FC<{ children: Stock }> = ({ children: stock }) => {
  const { stocks, setStock, removeStock } = useAppState();

  const totalWeight = stocks
    .filter(({ category }) => category === stock.category)
    .reduce((acc, { weight }) => acc + weight, 0);
  const percent = 100 * (stock.weight / totalWeight);

  return (
    <li>
      <span
        className="icon is-small remove"
        onClick={() => removeStock(stock.ticker)}
        role="button"
      >
        {"\u274c"}
      </span>
      <img src={stock.logo} alt={stock.ticker} />
      <strong className="has-text-white">{stock.ticker}</strong>
      <span className="ellipsis is-size-7 name">
        <abbr title={stock.name}>{stock.name}</abbr>
      </span>
      <span className="is-size-7">
        <abbr title={`${stock.weight} / ${totalWeight}`}>{`${percent.toFixed(
          2
        )}%`}</abbr>
      </span>
      <span
        className="icon is-small"
        onClick={() =>
          setStock(stock.ticker, ({ weight, ...stock }) => ({
            ...stock,
            weight: weight + 1,
          }))
        }
        role="button"
      >
        {"\u2795"}
      </span>
      <span
        className="icon is-small"
        onClick={() =>
          setStock(stock.ticker, ({ weight, ...stock }) => ({
            ...stock,
            weight: Math.max(weight - 1, 0),
          }))
        }
        role="button"
      >
        {"\u2796"}
      </span>
    </li>
  );
};

const SearchTicker: React.FC = () => {
  const [displayError, setDisplayError] = useState<Error | null>(null);

  const { addStock } = useAppState();
  const [value, setValue] = useState("");
  const [{ loading, error = null }, onSubmit] = useAsyncFn(async () => {
    await addStock(value);
    setValue("");
  }, [addStock, value]);
  useEffect(() => setDisplayError(error), [error, setDisplayError]);

  return (
    <form
      className="search"
      onSubmit={(e) => {
        e.preventDefault();
        if (value) onSubmit();
      }}
    >
      <div className="field">
        <div className={`control ${loading ? "is-loading" : ""}`}>
          <input
            className="input"
            inputMode="search"
            placeholder="Search ticker"
            value={value}
            onChange={(e) => setValue(e.currentTarget.value)}
            disabled={loading}
          />
        </div>
      </div>
      {displayError && (
        <div className="notification is-danger is-light">
          <button className="delete" onClick={() => setDisplayError(null)} />
          {displayError.message}
        </div>
      )}
    </form>
  );
};

const Export: React.FC = () => {
  const { exportState } = useAppState();
  const onExport = useCallback(() => {
    const state = exportState();
    const a = document.createElement("a");
    a.download = "margot.json";
    a.href = URL.createObjectURL(
      new Blob([JSON.stringify(state)], {
        type: "application/json",
      })
    );
    a.target = "_blank";
    a.addEventListener(
      "click",
      () => {
        setImmediate(URL.revokeObjectURL, a.download);
      },
      { passive: true, once: true }
    );
    a.click();
  }, [exportState]);

  return (
    <div>
      <button className="button is-light is-primary" onClick={onExport}>
        Export
      </button>
    </div>
  );
};

const Import: React.FC = () => {
  const { importState } = useAppState();
  const onImport = useCallback(
    ({ currentTarget }: SyntheticEvent<HTMLInputElement>) => {
      const file = currentTarget.files?.item(0);
      if (!file) return;

      const reader = new FileReader();
      reader.addEventListener("load", ({ target }) => {
        try {
          importState(JSON.parse(target?.result as string));
        } catch (err) {
          alert((err as Error).message);
        }
      });
      reader.readAsText(file);
    },
    [importState]
  );

  return (
    <div>
      <label className="button is-light is-primary">
        <input type="file" onChange={onImport} />
        Import
      </label>
    </div>
  );
};
