import "./graph.scss";
import React, { useMemo } from "react";
import Hashids from "hashids";
import Chart from "react-chartist";
import { useCss } from "react-use";
import { useAppState, useSuggestion } from "../../contexts";

export const Graphs: React.FC = () => {
  const className = useCss({
    display: "flex",
    gridArea: "graphs",
    margin: "1rem",
    "@media (max-aspect-ratio: 1/1)": { flexDirection: "column" },

    "> *": {
      position: "relative",
      height: "100%",
      width: "100%",

      "> h1": {
        fontWeight: 700,
        textAlign: "center",
      },
      ".ct-chart": {
        position: "absolute",
        height: "calc(100% - 24px)",
        width: "100%",
      },
      ".ct-label": {
        fontWeight: 700,
        textShadow:
          "1px 0 2px #333, -1px 0 2px #333, 0 1px 2px #333, 0 -1px 2px #333",
        userSelect: "none",
      },
    },
  });

  return (
    <div className={className}>
      <CurrentAllocation />
      <FutureAllocation />
    </div>
  );
};

const CurrentAllocation: React.FC = () => {
  const { categories, stocks, getPrice } = useAppState();
  const [categoryData, stockData] = useMemo(
    () => [
      categories
        .map(({ name }) => ({
          name,
          value: stocks
            .filter(
              ({ category, shares = 0 }) => category === name && shares !== 0
            )
            .reduce((prev, { ticker, shares }) => {
              const { value = 0 } = getPrice(ticker) ?? {};
              return prev + shares * value;
            }, 0),
        }))
        .filter(({ value }) => value !== 0)
        .sort((a, b) => a.name.localeCompare(b.name)),
      stocks
        .filter(({ shares = 0 }) => shares !== 0)
        .sort(
          (a, b) =>
            a.category.localeCompare(b.category) || a.name.localeCompare(b.name)
        )
        .map(({ ticker, shares }) => {
          const { value = 0 } = getPrice(ticker) ?? {};
          return {
            name: ticker,
            value: shares * value,
          };
        }),
    ],
    [categories, getPrice, stocks]
  );

  return (
    <section>
      <h1>Current allocation</h1>
      <Donut rings={[stockData, categoryData]} />
    </section>
  );
};

const FutureAllocation: React.FC = () => {
  const { categories, stocks, getPrice } = useAppState();
  const { getToBuy } = useSuggestion();

  const futureStocks = useMemo(
    () =>
      stocks
        .map(({ shares = 0, ...stock }) => ({
          ...stock,
          shares: shares + getToBuy(stock.ticker),
        }))
        .filter(({ shares = 0 }) => shares !== 0),
    [getToBuy, stocks]
  );
  const [categoryData, stockData] = useMemo(
    () => [
      categories
        .map(({ name }) => ({
          name,
          value: futureStocks
            .filter(({ category }) => category === name)
            .reduce((prev, { ticker, shares }) => {
              const { value = 0 } = getPrice(ticker) ?? {};
              return prev + shares * value;
            }, 0),
        }))
        .filter(({ value }) => value !== 0)
        .sort((a, b) => a.name.localeCompare(b.name)),
      [...futureStocks]
        .sort(
          (a, b) =>
            a.category.localeCompare(b.category) || a.name.localeCompare(b.name)
        )
        .map(({ ticker, shares }) => {
          const { value = 0 } = getPrice(ticker) ?? {};
          return {
            name: ticker,
            value: shares * value,
          };
        }),
    ],
    [categories, futureStocks, getPrice]
  );

  return (
    <section>
      <h1>Future allocation</h1>
      <Donut rings={[stockData, categoryData]} />
    </section>
  );
};

type ChartData = {
  name: string;
  value: number;
}[];
const Donut: React.FC<{ rings: ChartData[] }> = ({ rings }) => {
  const className = useCss(
    Object.fromEntries(
      rings.map((data, index) => [
        `&:nth-of-type(${index + 1})`,
        Object.fromEntries(
          data.map(({ name }, index) => [
            `.ct-series:nth-of-type(${index + 1}) .ct-slice-donut`,
            { stroke: `#${color(name)}` },
          ])
        ),
      ])
    )
  );

  return (
    <>
      {rings.map((data, index) => (
        <Chart
          key={data.map(({ name, value }) => `${name}${value}`).join("")}
          className={className}
          data={{
            labels: data.map(({ name }) => name),
            series: data.map(({ value }) => value),
          }}
          options={{
            chartPadding: 20 + index * 60,
            donut: true,
          }}
          type="Pie"
        />
      ))}
    </>
  );
};

const hashids = new Hashids("", 6, "abcdef1234567890");
const color = (seed: string) =>
  hashids.encode(Array.from(seed).map((s) => s.charCodeAt(0))).slice(0, 6);
