import React, { Suspense, lazy } from "react";
import { useCss } from "react-use";
import { FinnhubProvider } from "./contexts";

const View = lazy(() => import("./View"));

export const App: React.FC = () => {
  const className = useCss({
    display: "grid",
    gridTemplateColumns: "minmax(20rem, 1fr) 4fr",
    gridTemplateRows: "4fr minmax(16rem, 1fr)",
    gridTemplateAreas: '"aside graphs" "aside prices"',
    height: "100vh",
    minHeight: "600px",
    width: "100vw",
    overflow: "hidden",

    abbr: { textDecoration: "none" },
    ".ellipsis": {
      overflow: "hidden",
      textOverflow: "ellipsis",
      whiteSpace: "nowrap",
    },
    ".icon": {
      cursor: "pointer",
      userSelect: "none",
      flexShrink: 0,
      transition: "0.15s color",
    },
  });

  return (
    <FinnhubProvider>
      <div className={className}>
        <Suspense fallback={null}>
          <View />
        </Suspense>
      </div>
    </FinnhubProvider>
  );
};
